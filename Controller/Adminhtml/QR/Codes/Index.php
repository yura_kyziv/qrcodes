<?php
declare(strict_types=1);

namespace Mastering\QrCodes\Controller\Adminhtml\QR\Codes;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Admin store locator index page action
 */
class Index extends Action
{

    public const ADMIN_RESOURCE = "Magento_Backend::system";


    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
    ) {
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Mastering_QrCodes::menu')
            ->addBreadcrumb(__('QrCodes'), __('List'));
        $resultPage->getConfig()->getTitle()->prepend(__('QrCodes'));

        return $resultPage;
    }

}
